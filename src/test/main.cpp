#define CATCH_CONFIG_MAIN

#include <catch.hpp>
#include <thread>
#include <mutex>
#include <random>
#include <atomic>
#include <cstdlib>

extern "C" {
    #include <spsc_rbuf.h>
    #include <mpsc_rbuf.h>
    #include <normal_rbuf.h>
}

TEST_CASE("lock-free spsc ring buffer", "[spsc_rbuf]") {
    int64_t capacity = 200;
    spsc_rbuf_t *rb = spsc_rbuf_initialize(capacity);
    spsc_rbuf_chunk_t chunk;
    std::random_device rd;

    SECTION("full & empty") {
        spsc_rbuf_reset(rb);

        REQUIRE(spsc_rbuf_acquire(rb, 50, &chunk));
        REQUIRE(spsc_rbuf_produce(rb, &chunk));

        REQUIRE(spsc_rbuf_acquire(rb, 50, &chunk));
        REQUIRE(spsc_rbuf_produce(rb, &chunk));

        REQUIRE_FALSE(spsc_rbuf_acquire(rb, 100, &chunk));

        REQUIRE(spsc_rbuf_read(rb, &chunk));
        REQUIRE(spsc_rbuf_consume(rb, &chunk));

        REQUIRE(spsc_rbuf_size(rb) > 0);

        REQUIRE(spsc_rbuf_read(rb, &chunk));
        REQUIRE(spsc_rbuf_consume(rb, &chunk));

        REQUIRE(spsc_rbuf_size(rb) == 0);

        REQUIRE_FALSE(spsc_rbuf_read(rb, &chunk));
    }

    SECTION("rotation") {
        spsc_rbuf_reset(rb);
        std::mt19937_64 gen(rd());
        std::uniform_int_distribution<> dis(sizeof(char) * 5, sizeof(char) * 10);
        for (int i = 0; i < 100; i++) {
            auto size = dis(gen);

            REQUIRE(spsc_rbuf_acquire(rb, size, &chunk));
            REQUIRE(spsc_rbuf_produce(rb, &chunk));

            REQUIRE(spsc_rbuf_read(rb, &chunk));
            REQUIRE(chunk.size == size);
            REQUIRE(spsc_rbuf_consume(rb, &chunk));

            REQUIRE(spsc_rbuf_size(rb) == 0);
        }
    }

    SECTION("threading") {
        spsc_rbuf_reset(rb);
        std::atomic<bool> running = true;
        auto test_count = 100;
        auto read_count = 0;

        auto enqueue = std::thread([&]{
            spsc_rbuf_chunk_t chunk;
            std::mt19937_64 gen(rd());
            std::uniform_int_distribution<> dis(sizeof(char) * 5, sizeof(char) * 10);

            for (int i = 0; i < test_count; i++) {
                auto size = dis(gen);
                while (!spsc_rbuf_acquire(rb, size, &chunk)) {
                    if (!running) {
                        return;
                    }
                    std::this_thread::yield();
                }
                sprintf_s(chunk.data, size, "%d", i);
                if (!spsc_rbuf_produce(rb, &chunk)) {
                    std::cout << "enqueue break at " << i << std::endl;
                    break;
                }
            }
            std::cout << "enqueue finished" << std::endl;
            running = false;
        });
        auto dequeue = std::thread([&]{
            spsc_rbuf_chunk_t chunk;
            for (;;) {
                if (spsc_rbuf_read(rb, &chunk)) {
                    auto v = std::atoi(chunk.data);
                    if (v != read_count) {
                        std::cout << "wrong data at " << read_count
                                  << " value: " << chunk.data << std::endl;
                        int64_t head, tail;
                        spsc_rbuf_status(rb, &head, &tail);
                        break;
                    }

                    spsc_rbuf_consume(rb, &chunk);
                    read_count++;
                } else {
                    if (!running) {
                        std::cout << "dequeue break at " << read_count << std::endl;
                        break;
                    }
                    std::this_thread::yield();
                }
            }
            running = false;
            std::cout << "dequeue finished" << std::endl;
        });

        enqueue.join();
        dequeue.join();

        REQUIRE(read_count == test_count);
    }

    spsc_rbuf_destroy(rb);
}

TEST_CASE("lock-free mpsc ring buffer", "[lockfree_rbuf]") {
    int64_t capacity = 1024;
    mpsc_rbuf_t *rb = mpsc_rbuf_initialize(capacity);
    std::random_device rd;

    SECTION("threading") {
        const auto test_count = 10000;
        const auto thread_count = 10;
        auto read_count = 0;
        std::atomic<int> index = 0;
        std::vector<std::thread> threads;
        threads.reserve(thread_count);

        std::atomic<int> running_enq = 0;
        std::atomic<bool> running_deq = true;
        std::atomic<int64_t> pushed_id_sum = 0;
        std::atomic<int64_t> read_id_sum = 0;

        auto enqueue_func = [&](int id){
            mpsc_rbuf_acquired_chunk_t chunk;
            std::mt19937_64 gen(rd());
            std::uniform_int_distribution<> dis(sizeof(char) * 5, sizeof(char) * 100);

            for (;;) {
                auto i = index.fetch_add(1);
                if (i < test_count) {
                    auto size = dis(gen);
                    while (!mpsc_rbuf_acquire(rb, size, &chunk)) {
                        if (!running_deq.load()) {
                            return;
                        }
                        std::this_thread::yield();
                    }
                    int *data = (int*)(chunk.data);
                    *data = i + 1;
                    pushed_id_sum += *data;
                    mpsc_rbuf_push(rb, &chunk);
                    // std::cout << "enq" << id << " push " << i << std::endl;
                } else {
                    break;
                }
            }
            std::cout << "enq" << id << " finished" << std::endl;
        };

        for (int i = 0; i < thread_count; i++) {
            running_enq.fetch_add(1);
            std::cout << "enq" << i << " start" << std::endl;
            threads.emplace_back(std::thread([&]{
                enqueue_func(i);
                running_enq.fetch_sub(1);
            }));
        }

        auto deq = std::thread([&]{
            mpsc_rbuf_chunk_t chunk;
            for (;;) {
                if (mpsc_rbuf_read(rb, &chunk)) {
                    int *data = (int*)(chunk.data);
                    read_id_sum += *data;
                    read_count++;
                    if (!mpsc_rbuf_pop(rb)) {
                        std::cout << "failed to pop after read" << std::endl;
                    }
                } else {
                    if (running_enq.load() == 0) {
                        break;
                    }
                    std::this_thread::yield();
                }
            }
            std::cout << "deq finished" << std::endl;
            running_deq = false;
        });

        for (auto &t: threads) {
            t.join();
        }
        deq.join();

        REQUIRE(read_count == test_count);
        REQUIRE(pushed_id_sum == read_id_sum);

        std::cout << "sum " << 1 << " to " << test_count << " is " << read_id_sum << std::endl;
    }

    SECTION("blocking") {
        const auto test_count = 100;
        const auto thread_count = 10;
        auto read_count = 0;
        std::atomic<int> index = 0;
        std::vector<std::thread> threads;
        threads.reserve(thread_count);

        std::atomic<int> running_enq = 0;
        std::atomic<bool> running_deq = true;
        std::atomic<int64_t> pushed_id_sum = 0;
        std::atomic<int64_t> read_id_sum = 0;

        auto enqueue_func = [&](int id){
            mpsc_rbuf_acquired_chunk_t chunk;
            std::mt19937_64 gen(rd());
            std::uniform_int_distribution<> dis(sizeof(char) * 5, sizeof(char) * 100);

            for (;;) {
                auto i = index.fetch_add(1);
                if (i < test_count) {
                    auto size = dis(gen);
                    while (!mpsc_rbuf_acquire(rb, size, &chunk)) {
                        if (!running_deq.load()) {
                            return;
                        }
                        std::this_thread::yield();
                    }
                    int *data = (int*)(chunk.data);
                    *data = i + 1;
                    pushed_id_sum += *data;
                    mpsc_rbuf_push(rb, &chunk);
                    mpsc_rbuf_wakeup(rb);

                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                } else {
                    break;
                }
            }
            std::cout << "enq" << id << " finished" << std::endl;
        };

        for (int i = 0; i < thread_count; i++) {
            running_enq.fetch_add(1);
            std::cout << "enq" << i << " start" << std::endl;
            threads.emplace_back(std::thread([&]{
                enqueue_func(i);
                running_enq.fetch_sub(1);

                mpsc_rbuf_wakeup(rb);
            }));
        }

        auto deq = std::thread([&]{
            mpsc_rbuf_chunk_t chunk;
            for (;;) {
                if (mpsc_rbuf_read(rb, &chunk)) {
                    int *data = (int*)(chunk.data);
                    read_id_sum += *data;
                    read_count++;
                    if (!mpsc_rbuf_pop(rb)) {
                        std::cout << "failed to pop after read" << std::endl;
                    }
                } else {
                    if (running_enq.load() == 0) {
                        break;
                    }
                    mpsc_rbuf_wait(rb);
                }
            }
            std::cout << "deq finished" << std::endl;
            running_deq = false;
        });

        for (auto &t: threads) {
            t.join();
        }
        deq.join();

        REQUIRE(read_count == test_count);
        REQUIRE(pushed_id_sum == read_id_sum);

        std::cout << "sum " << 1 << " to " << test_count << " is " << read_id_sum << std::endl;
    }

    mpsc_rbuf_destroy(rb);
}

TEST_CASE("mpsc ring buffer with mutex", "[rbuf_with_mutex]") {
    int64_t capacity = 1024;
    normal_rbuf_t *rb = normal_rbuf_initialize(capacity);
    std::random_device rd;

    SECTION("temp") {
        REQUIRE(normal_rbuf_size(rb) == 0);
    }

    SECTION("threading") {
        const auto test_count = 10000;
        const auto thread_count = 10;
        auto read_count = 0;
        std::atomic<int> index = 0;
        std::vector<std::thread> threads;
        threads.reserve(thread_count);

        std::atomic<int> running_enq = 0;
        std::atomic<bool> running_deq = true;
        std::atomic<int64_t> pushed_id_sum = 0;
        std::atomic<int64_t> read_id_sum = 0;

        auto enqueue_func = [&](int id){
            normal_rbuf_acquired_chunk_t chunk;
            std::mt19937_64 gen(rd());
            std::uniform_int_distribution<> dis(sizeof(char) * 5, sizeof(char) * 100);

            for (;;) {
                auto i = index.fetch_add(1);
                if (i < test_count) {
                    auto size = dis(gen);
                    while (!normal_rbuf_acquire2(rb, size, &chunk)) {
                        if (!running_deq.load()) {
                            return;
                        }
                        std::this_thread::yield();
                    }
                    int *data = (int*)(chunk.data);
                    *data = i + 1;
                    pushed_id_sum += *data;
                    normal_rbuf_push2(rb, &chunk);
                    // std::cout << "enq" << id << " push " << i << std::endl;
                } else {
                    break;
                }
            }
            std::cout << "enq" << id << " finished" << std::endl;
        };

        for (int i = 0; i < thread_count; i++) {
            running_enq.fetch_add(1);
            std::cout << "enq" << i << " start" << std::endl;
            threads.emplace_back(std::thread([&]{
                enqueue_func(i);
                running_enq.fetch_sub(1);
            }));
        }

        auto deq = std::thread([&]{
            normal_rbuf_chunk_t chunk;
            for (;;) {
                if (normal_rbuf_read(rb, &chunk)) {
                    int *data = (int*)(chunk.data);
                    read_id_sum += *data;
                    read_count++;
                    if (!normal_rbuf_pop(rb)) {
                        std::cout << "failed to pop after read" << std::endl;
                    }
                } else {
                    if (running_enq.load() == 0) {
                        break;
                    }
                    std::this_thread::yield();
                }
            }
            std::cout << "deq finished" << std::endl;
            running_deq = false;
        });

        for (auto &t: threads) {
            t.join();
        }
        deq.join();

        REQUIRE(read_count == test_count);
        REQUIRE(pushed_id_sum == read_id_sum);

        std::cout << "sum " << 1 << " to " << test_count << " is " << read_id_sum << std::endl;
    }

    normal_rbuf_destroy(rb);
}