#define NONIUS_RUNNER
#include <nonius/nonius_single.h++>

#include <list>
#include <forward_list>

#include "spsc.hpp"
#include "mpsc.hpp"

void fill_dummy(char* data, int size)
{
    for (int i = 0; i < size; i++) {
        data[i] = 'a' + rand() % 26;
    }
}

NONIUS_BENCHMARK("0.spsc(1000, 100B/10MB)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    spsc_rbuf_t *rb = spsc_rbuf_initialize(10 * 1024);
    test_spsc(rb, 1000, 100, data, meter);
    spsc_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("1.mpsc(1000, 100B/10MB, 1 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    mpsc_rbuf_t *rb = mpsc_rbuf_initialize(10 * 1024);
    test_mpsc(rb, 1000, 100, 1, data, meter);
    mpsc_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("2.mpsc_with_mutex(1000, 100B/10MB, 1 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    normal_rbuf_t *rb = normal_rbuf_initialize(10 * 1024);
    test_mpsc_with_mutex(rb, 1000, 100, 1, data, meter);
    normal_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("3.mpsc(1000, 100B/10MB, 4 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    mpsc_rbuf_t *rb = mpsc_rbuf_initialize(10 * 1024);
    test_mpsc(rb, 1000, 100, 4, data, meter);
    mpsc_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("4.mpsc_with_mutex(1000, 100B/10MB, 4 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    normal_rbuf_t *rb = normal_rbuf_initialize(10 * 1024);
    test_mpsc_with_mutex(rb, 1000, 100, 4, data, meter);
    normal_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("5.mpsc(1000, 100B/10MB, 8 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    mpsc_rbuf_t *rb = mpsc_rbuf_initialize(10 * 1024);
    test_mpsc(rb, 1000, 100, 8, data, meter);
    mpsc_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("6.mpsc_with_mutex(1000, 100B/10MB, 8 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    normal_rbuf_t *rb = normal_rbuf_initialize(10 * 1024);
    test_mpsc_with_mutex(rb, 1000, 100, 8, data, meter);
    normal_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("7.mpsc(1000, 100B/10MB, 16 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    mpsc_rbuf_t *rb = mpsc_rbuf_initialize(10 * 1024);
    test_mpsc(rb, 1000, 100, 16, data, meter);
    mpsc_rbuf_destroy(rb);
})

NONIUS_BENCHMARK("8.mpsc_with_mutex(1000, 100B/10MB, 16 threads)", [](nonius::chronometer meter) {
    char data[100];
    fill_dummy(data, 100);
    normal_rbuf_t *rb = normal_rbuf_initialize(10 * 1024);
    test_mpsc_with_mutex(rb, 1000, 100, 16, data, meter);
    normal_rbuf_destroy(rb);
})