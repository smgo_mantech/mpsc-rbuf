#pragma once

extern "C" {
    #include <normal_rbuf.h>
    #include <mpsc_rbuf.h>
}

void test_mpsc(
    mpsc_rbuf_t *rb, int test_count, int block_size, int thread_count,
    char *data, nonius::chronometer meter
) {
    std::vector<std::thread> threads;
    threads.reserve(thread_count);

    std::atomic<int> running_enq = thread_count;
    std::atomic<bool> running_deq = true;
    std::atomic<bool> standby = false;

     auto enqueue_func = [&](int id, int count){
        while (standby)
            std::this_thread::yield();

        mpsc_rbuf_acquired_chunk_t chunk;
        for (int i = 0; i < count; i++) {
            while (!mpsc_rbuf_acquire(rb, block_size, &chunk)) {
                std::this_thread::yield();
            }
            memcpy_s(chunk.data, chunk.size, data, block_size);
            mpsc_rbuf_push(rb, &chunk);
        }
        running_enq.fetch_sub(1);
    };

    auto deq = std::thread([&]{
        mpsc_rbuf_chunk_t chunk;
        for (;;) {
            if (mpsc_rbuf_read(rb, &chunk)) {
                if (!mpsc_rbuf_pop(rb)) {
                    std::cout << "failed to pop after read" << std::endl;
                }
            } else {
                if (running_enq.load() == 0) {
                    break;
                }
                std::this_thread::yield();
            }
        }
        running_deq = false;
    });

    auto count_per_thread = test_count / thread_count;
    auto remained = test_count - (count_per_thread * thread_count);
    for (int i = 0; i < thread_count; i++) {
        auto count = count_per_thread;
        if (remained > 0) {
            count++;
            remained--;
        }
        threads.emplace_back(std::thread([&, i, count]{
            enqueue_func(i, count);
        }));
    }

    meter.measure([&](int i) {
        standby = false;
        deq.join();
    });

    for (auto &t: threads) {
        t.join();
    }
}

void test_mpsc_with_mutex(
    normal_rbuf_t *rb, int test_count, int block_size, int thread_count,
    char *data, nonius::chronometer meter
) {
    std::vector<std::thread> threads;
    threads.reserve(thread_count);

    std::atomic<int> running_enq = thread_count;
    std::atomic<bool> running_deq = true;
    std::atomic<bool> standby = false;

    auto enqueue_func = [&](int id, int count){
        while (standby)
            std::this_thread::yield();

        normal_rbuf_acquired_chunk_t chunk;
        for (int i = 0; i < count; i++) {
            while (!normal_rbuf_acquire2(rb, block_size, &chunk)) {
                std::this_thread::yield();
            }
            memcpy_s(chunk.data, chunk.size, data, block_size);
            normal_rbuf_push2(rb, &chunk);
        }
        running_enq.fetch_sub(1);
    };

    auto deq = std::thread([&]{
        normal_rbuf_chunk_t chunk;
        for (;;) {
            if (normal_rbuf_read(rb, &chunk)) {
                if (!normal_rbuf_pop(rb)) {
                    std::cout << "failed to pop after read" << std::endl;
                }
            } else {
                if (running_enq.load() == 0) {
                    break;
                }
                std::this_thread::yield();
            }
        }
        running_deq = false;
    });

    auto count_per_thread = test_count / thread_count;
    auto remained = test_count - (count_per_thread * thread_count);
    for (int i = 0; i < thread_count; i++) {
        auto count = count_per_thread;
        if (remained > 0) {
            count++;
            remained--;
        }
        threads.emplace_back(std::thread([&, i, count]{
            enqueue_func(i, count);
        }));
    }

    meter.measure([&](int i) {
        standby = false;
        deq.join();
    });

    for (auto &t: threads) {
        t.join();
    }
}