#pragma once

#include <condition_variable>

extern "C" {
    #include <spsc_rbuf.h>
}

void test_spsc(
    spsc_rbuf_t *rb, int test_count, int block_size, char *data,
    nonius::chronometer meter
) {
    spsc_rbuf_reset(rb);
    std::atomic<bool> running = true;
    auto read_count = 0;

    auto dequeue = std::thread([&]{
        spsc_rbuf_chunk_t chunk;
        for (;;) {
            if (spsc_rbuf_read(rb, &chunk)) {
                spsc_rbuf_consume(rb, &chunk);
                read_count++;
            } else {
                if (!running) {
                    break;
                }
                std::this_thread::yield();
            }
        }
        running = false;
    });

    std::condition_variable cv;
    std::mutex m;

    auto enqueue = std::thread([&]{
        std::unique_lock<std::mutex> lock(m);
        cv.wait(lock);

        spsc_rbuf_chunk_t chunk;
        for (int i = 0; i < test_count; i++) {
            while (!spsc_rbuf_acquire(rb, block_size, &chunk)) {
                std::this_thread::yield();
            }
            memcpy_s(chunk.data, chunk.size, data, block_size);
            if (!spsc_rbuf_produce(rb, &chunk)) {
                break;
            }
        }
        running = false;
    });

    meter.measure([&](int i) {
        cv.notify_all();
        dequeue.join();
    });
    enqueue.join();
}