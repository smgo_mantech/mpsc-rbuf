include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

set(BOOST_COMPONENTS_NEEDED
    chrono
)
find_package(Boost REQUIRED COMPONENTS ${BOOST_COMPONENTS_NEEDED})

if(Boost_FOUND)
    MESSAGE(STATUS "Setting up boost.")
    include_directories(${Boost_INCLUDE_DIRS})
    link_directories(${Boost_LIBRARY_DIRS})

    if(Boost_DEBUG) 
        MESSAGE(STATUS "BOOST Libraries " ${Boost_LIBRARIES})
        FOREACH(BOOST_COMPONENT ${BOOST_COMPONENTS_NEEDED})
            STRING(TOUPPER ${BOOST_COMPONENT} BOOST_COMPONENT_UPCASE)
            MESSAGE(STATUS "Boost " ${BOOST_COMPONENT} ": " ${Boost_${BOOST_COMPONENT_UPCASE}_LIBRARY})
            MESSAGE(STATUS "Boost " ${BOOST_COMPONENT} " Debug: " ${Boost_${BOOST_COMPONENT_UPCASE}_LIBRARY_DEBUG})
            MESSAGE(STATUS "Boost " ${BOOST_COMPONENT} " Release: " ${Boost_${BOOST_COMPONENT_UPCASE}_LIBRARY_RELEASE})
        ENDFOREACH(BOOST_COMPONENT)
    endif(Boost_DEBUG)
endif(Boost_FOUND)

add_executable(bench
    main.cpp
    spsc.hpp
    mpsc.hpp
)

target_include_directories(bench
    PUBLIC ${CMAKE_SOURCE_DIR}/lib/rbuf
    PUBLIC ${CMAKE_SOURCE_DIR}/external
)

target_link_libraries(bench
    ${Boost_LIBRARIES} rbuf
)