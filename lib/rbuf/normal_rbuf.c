#include "normal_rbuf.h"

#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>

/**
 * @brief normal_rbuf MPSC Ring buffer with mutex
 */
struct normal_rbuf {
    int64_t capacity;
    char   *buf;
    HANDLE  mutex;

    volatile int64_t acquired;
    volatile int64_t pushed;
    volatile int64_t read;
    volatile int64_t poped;
};

enum normal_rbuf_state {
    ACQUIRED, OMMITED
};

#pragma pack(push, 1)
typedef struct normal_rbuf_header {
    int64_t              size;
    enum normal_rbuf_state state;
} normal_rbuf_header_t;
#pragma pack(pop)

static int64_t normal_rbuf_header_size = sizeof(normal_rbuf_header_t);

normal_rbuf_t *normal_rbuf_initialize(int64_t capacity)
{
    normal_rbuf_t *rb = (normal_rbuf_t*)malloc(sizeof(normal_rbuf_t));
    rb->buf = (char*)malloc(sizeof(char) * capacity);
    rb->capacity = capacity;

    rb->mutex = CreateMutex(NULL, FALSE, NULL);

    normal_rbuf_reset(rb);
    return rb;
}

void normal_rbuf_destroy(normal_rbuf_t *rb)
{
    if (rb != NULL) {
        if (rb->buf != NULL) {
            free(rb->buf);
        }
        free(rb);

        if (rb->mutex != NULL) {
            CloseHandle(rb->mutex);
        }
    }
}

int64_t normal_rbuf_capacity(normal_rbuf_t *rb)
{
    return rb->capacity;
}

int64_t normal_rbuf_size(normal_rbuf_t *rb)
{
    int64_t size = 0;
    WaitForSingleObject(rb->mutex, INFINITE);
    if (rb->acquired > rb->poped) {
        size = rb->acquired - rb->poped;
    } else if (rb->acquired < rb->poped) {
        size = rb->capacity - (rb->poped - rb->acquired);
    }
    ReleaseMutex(rb->mutex);
    return size;
}

void normal_rbuf_reset(normal_rbuf_t *rb)
{
    WaitForSingleObject(rb->mutex, INFINITE);
    rb->acquired = 0;
    rb->pushed = 0;
    rb->read = 0;
    rb->poped = 0;
    ReleaseMutex(rb->mutex);
}

bool normal_rbuf_acquire(normal_rbuf_t *rb, int64_t size, normal_rbuf_acquired_chunk_t *chunk)
{
    int64_t raw_size = size + normal_rbuf_header_size;
    normal_rbuf_header_t *header;

    WaitForSingleObject(rb->mutex, INFINITE);

    int64_t next = rb->acquired + raw_size;

    if (rb->acquired < rb->poped) { // [==a--p==]
        if (next < rb->poped) { // 기록 가능
            header = (normal_rbuf_header_t*)(rb->buf + rb->acquired);
            header->size = size;
            header->state = ACQUIRED;

            chunk->data = rb->buf + rb->acquired + normal_rbuf_header_size;
            chunk->size = size;
            chunk->_prev = rb->acquired;
            chunk->_next = next;

            rb->acquired = next;

            // printf("acq exchanged1 %d to %d:%d\n", rb->acquired, next, size);
            ReleaseMutex(rb->mutex);
            return true;
        } else { // full
            // printf("full1\n");
            ReleaseMutex(rb->mutex);
            return false;
        }
    } else { // [--p==a--] or empty
        if (next + normal_rbuf_header_size >= rb->capacity) { // 포인터 오버플로우
            if (raw_size < rb->poped) {
                next = raw_size;

                header = (normal_rbuf_header_t*)(rb->buf + rb->acquired);
                header->size = rb->capacity - rb->acquired;
                header->state = OMMITED;

                // printf("acq omitted %d\n", acq);

                header = (normal_rbuf_header_t*)(rb->buf);
                header->size = size;
                header->state = ACQUIRED;

                chunk->data = rb->buf + normal_rbuf_header_size;
                chunk->size = size;
                chunk->_prev = rb->acquired;
                chunk->_next = next;

                // printf("acq exchanged2 %d to %d:%d\n", rb->acquired, next, size);
                rb->acquired = next;
                ReleaseMutex(rb->mutex);
                return true;
            } else { // full
                // printf("full2\n");
                ReleaseMutex(rb->mutex);
                return false;
            }
        } else {
            header = (normal_rbuf_header_t*)(rb->buf + rb->acquired);
            header->size = size;
            header->state = ACQUIRED;

            chunk->data = rb->buf + rb->acquired + normal_rbuf_header_size;
            chunk->size = size;
            chunk->_prev = rb->acquired;
            chunk->_next = next;

            // printf("acq exchanged3 %d to %d:%d\n", rb->acquired, next, size);
            rb->acquired = next;
            ReleaseMutex(rb->mutex);
            return true;
        }
    }

    ReleaseMutex(rb->mutex);
    return false;
}

bool normal_rbuf_push(normal_rbuf_t *rb, normal_rbuf_acquired_chunk_t *chunk)
{
    WaitForSingleObject(rb->mutex, INFINITE);
    while (rb->pushed != chunk->_prev) {
        ReleaseMutex(rb->mutex);
        SwitchToThread();
        WaitForSingleObject(rb->mutex, INFINITE);
    }
    rb->pushed = chunk->_next;
    ReleaseMutex(rb->mutex);
    return true;
}

bool normal_rbuf_acquire2(normal_rbuf_t *rb, int64_t size, normal_rbuf_acquired_chunk_t *chunk)
{
    int64_t raw_size = size + normal_rbuf_header_size;
    normal_rbuf_header_t *header;

    WaitForSingleObject(rb->mutex, INFINITE);

    int64_t next = rb->acquired + raw_size;

    if (rb->acquired < rb->poped) { // [==a--p==]
        if (next < rb->poped) { // 기록 가능
            header = (normal_rbuf_header_t*)(rb->buf + rb->acquired);
            header->size = size;
            header->state = ACQUIRED;

            chunk->data = rb->buf + rb->acquired + normal_rbuf_header_size;
            chunk->size = size;
            chunk->_prev = rb->acquired;
            chunk->_next = next;

            rb->acquired = next;

            // printf("acq exchanged1 %d to %d:%d\n", rb->acquired, next, size);
            return true;
        } else { // full
            // printf("full1\n");
            ReleaseMutex(rb->mutex);
            return false;
        }
    } else { // [--p==a--] or empty
        if (next + normal_rbuf_header_size >= rb->capacity) { // 포인터 오버플로우
            if (raw_size < rb->poped) {
                next = raw_size;

                header = (normal_rbuf_header_t*)(rb->buf + rb->acquired);
                header->size = rb->capacity - rb->acquired;
                header->state = OMMITED;

                // printf("acq omitted %d\n", acq);

                header = (normal_rbuf_header_t*)(rb->buf);
                header->size = size;
                header->state = ACQUIRED;

                chunk->data = rb->buf + normal_rbuf_header_size;
                chunk->size = size;
                chunk->_prev = rb->acquired;
                chunk->_next = next;

                // printf("acq exchanged2 %d to %d:%d\n", rb->acquired, next, size);
                rb->acquired = next;
                return true;
            } else { // full
                // printf("full2\n");
                ReleaseMutex(rb->mutex);
                return false;
            }
        } else {
            header = (normal_rbuf_header_t*)(rb->buf + rb->acquired);
            header->size = size;
            header->state = ACQUIRED;

            chunk->data = rb->buf + rb->acquired + normal_rbuf_header_size;
            chunk->size = size;
            chunk->_prev = rb->acquired;
            chunk->_next = next;

            // printf("acq exchanged3 %d to %d:%d\n", rb->acquired, next, size);
            rb->acquired = next;
            return true;
        }
    }

    ReleaseMutex(rb->mutex);
    return false;
}

bool normal_rbuf_push2(normal_rbuf_t *rb, normal_rbuf_acquired_chunk_t *chunk)
{
    rb->pushed = chunk->_next;
    ReleaseMutex(rb->mutex);
    return true;
}

bool normal_rbuf_read(normal_rbuf_t *rb, normal_rbuf_chunk_t *chunk)
{
    WaitForSingleObject(rb->mutex, INFINITE);

    normal_rbuf_header_t *header;
    int64_t tail = rb->read;

    if (rb->pushed == tail) {
        ReleaseMutex(rb->mutex);
        return false;
    }

    if (rb->pushed > tail &&
        rb->pushed - tail < normal_rbuf_header_size) { // [---th---] => empty
        ReleaseMutex(rb->mutex);
        return false;
    }

    header = (normal_rbuf_header_t*)(rb->buf + tail);
    if (header->state == OMMITED) {
        // 이 상태는 포인터의 맨 끝에만 지정되므로 즉시 처음으로 이동
        if (rb->pushed == 0) { // empty
            ReleaseMutex(rb->mutex);
            return false;
        }
        tail = 0;
        header = (normal_rbuf_header_t*)(rb->buf);
        // printf("read: skip ommited\n");
    }

    int64_t next = tail + normal_rbuf_header_size + header->size;
    chunk->data = rb->buf + tail + normal_rbuf_header_size;
    chunk->size = header->size;

    // printf("read at %d:%d next %d\n", tail, header->size, next);
    rb->read = next;
    ReleaseMutex(rb->mutex);
    return true;
}

bool normal_rbuf_pop(normal_rbuf_t *rb)
{
    WaitForSingleObject(rb->mutex, INFINITE);

    normal_rbuf_header_t *header;
    int64_t tail = rb->poped;

    if (rb->pushed == tail) {
        // printf("empty1\n");
        ReleaseMutex(rb->mutex);
        return false;
    }

    if (rb->pushed > tail && rb->pushed - tail < normal_rbuf_header_size) { // [---th---] => empty
        // printf("empty2\n");
        ReleaseMutex(rb->mutex);
        return false;
    }

    header = (normal_rbuf_header_t*)(rb->buf + tail);
    if (header->state == OMMITED) {
        // 이 상태는 포인터의 맨 끝에만 지정되므로 즉시 처음으로 이동
        if (rb->pushed == 0) { // empty
            ReleaseMutex(rb->mutex);
            return false;
        }
        tail = 0;
        header = (normal_rbuf_header_t*)(rb->buf);
        // printf("skip ommited\n");
    }

    int64_t next = tail + normal_rbuf_header_size + header->size;
    // printf("poped at %d:%d next %d\n", tail, header->size, next);
    rb->poped = next;
    ReleaseMutex(rb->mutex);
    return true;
}
