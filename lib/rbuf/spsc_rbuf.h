#pragma once

#include <inttypes.h>
#include <stdbool.h>

typedef struct spsc_rbuf spsc_rbuf_t;
typedef struct spsc_rbuf_chunk {
    char   *data;
    int64_t size;
    int64_t _next;
} spsc_rbuf_chunk_t;

spsc_rbuf_t *spsc_rbuf_initialize(int64_t capacity);
void         spsc_rbuf_destroy(spsc_rbuf_t *rb);
int64_t      spsc_rbuf_capacity(spsc_rbuf_t *rb);
int64_t      spsc_rbuf_size(spsc_rbuf_t *rb);

void spsc_rbuf_reset(spsc_rbuf_t *rb);

bool spsc_rbuf_acquire(spsc_rbuf_t *rb, int64_t size, spsc_rbuf_chunk_t *chunk);
bool spsc_rbuf_produce(spsc_rbuf_t *rb, spsc_rbuf_chunk_t *chunk);

bool spsc_rbuf_read(spsc_rbuf_t *rb, spsc_rbuf_chunk_t *chunk);
bool spsc_rbuf_consume(spsc_rbuf_t *rb, spsc_rbuf_chunk_t *chunk);

void spsc_rbuf_status(spsc_rbuf_t *rb, int64_t *head, int64_t *tail);