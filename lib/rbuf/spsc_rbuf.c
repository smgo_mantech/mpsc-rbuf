#include "spsc_rbuf.h"

#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>

/**
 * @brief spsc_rbuf SPSC Ring buffer
 */
struct spsc_rbuf {
    int64_t capacity;
    char   *buf;

    volatile int64_t head;
    volatile int64_t tail;
};

enum spsc_rbuf_state {
    ACQUIRED, OMMITED
};

#pragma pack(push, 1)
typedef struct spsc_rbuf_header {
    int64_t              size;
    enum spsc_rbuf_state state;
} spsc_rbuf_header_t;
#pragma pack(pop)

static int64_t spsc_rbuf_header_size = sizeof(spsc_rbuf_header_t);

spsc_rbuf_t *spsc_rbuf_initialize(int64_t capacity)
{
    spsc_rbuf_t *rb = (spsc_rbuf_t*)malloc(sizeof(spsc_rbuf_t));
    rb->buf = (char*)malloc(sizeof(char) * capacity);
    rb->capacity = capacity;

    spsc_rbuf_reset(rb);
    return rb;
}

void spsc_rbuf_destroy(spsc_rbuf_t *rb)
{
    if (rb != NULL) {
        if (rb->buf != NULL) {
            free(rb->buf);
        }
        free(rb);
    }
}

int64_t spsc_rbuf_capacity(spsc_rbuf_t *rb)
{
    return rb->capacity;
}

int64_t spsc_rbuf_size(spsc_rbuf_t *rb)
{
    int64_t head = InterlockedCompareExchange64(&rb->head, 0, 0);
    int64_t tail = InterlockedCompareExchange64(&rb->tail, 0, 0);

    if (head > tail) {
        return head - tail;
    } else if (head < tail) {
        return rb->capacity - (tail - head);
    }
    return 0;
}

void spsc_rbuf_reset(spsc_rbuf_t *rb)
{
    InterlockedExchange64(&rb->head, 0);
    InterlockedExchange64(&rb->tail, 0);
}

bool spsc_rbuf_acquire(spsc_rbuf_t *rb, int64_t size, spsc_rbuf_chunk_t *chunk)
{
    int64_t raw_size = size + spsc_rbuf_header_size;
    int64_t head = InterlockedCompareExchange64(&rb->head, 0, 0);
    int64_t tail = InterlockedCompareExchange64(&rb->tail, 0, 0);
    spsc_rbuf_header_t *header;

    // 다음 위치 계산
    int64_t next = head + raw_size;

    // 상태 별 처리
    if (head < tail) { // [==h--t==]
        if (next < tail) { // 기록 가능
            header = (spsc_rbuf_header_t*)(rb->buf + head);
            header->size = size;
            header->state = ACQUIRED;

            chunk->data = rb->buf + head + spsc_rbuf_header_size;
            chunk->size = size;
            chunk->_next = next;
        } else { // full
            return false;
        }
    } else { // [--t==h--] or empty
        if (next + spsc_rbuf_header_size >= rb->capacity) { // 포인터 오버플로우
            if (raw_size < tail) {
                // 뒷 부분 마킹
                header = (spsc_rbuf_header_t*)(rb->buf + head);
                header->size = rb->capacity - head;
                header->state = OMMITED;

                header = (spsc_rbuf_header_t*)(rb->buf);
                header->size = size;
                header->state = ACQUIRED;

                chunk->data = rb->buf + spsc_rbuf_header_size;
                chunk->size = size;
                chunk->_next = raw_size;
            } else { // full
                return false;
            }
        } else {
            header = (spsc_rbuf_header_t*)(rb->buf + head);
            header->size = size;
            header->state = ACQUIRED;

            chunk->data = rb->buf + head + spsc_rbuf_header_size;
            chunk->size = size;
            chunk->_next = next;
        }
    }
    return true;
}

bool spsc_rbuf_produce(spsc_rbuf_t *rb, spsc_rbuf_chunk_t *chunk)
{
    InterlockedExchange64(&rb->head, chunk->_next);
    return true;
}

bool spsc_rbuf_read(spsc_rbuf_t *rb, spsc_rbuf_chunk_t *chunk)
{
    int64_t head = InterlockedCompareExchange64(&rb->head, 0, 0);
    int64_t tail = InterlockedCompareExchange64(&rb->tail, 0, 0);
    spsc_rbuf_header_t *header;

    if (head == tail) {
        return false;
    }

    if (head > tail && head - tail < spsc_rbuf_header_size) { // [---th---] => empty
        return false;
    }

    header = (spsc_rbuf_header_t*)(rb->buf + tail);
    if (header->state == OMMITED) {
        // 이 상태는 포인터의 맨 끝에만 지정되므로 즉시 처음으로 이동
        if (head == 0) { // empty
            return false;
        }
        tail = 0;
        header = (spsc_rbuf_header_t*)(rb->buf);
    }

    chunk->data = rb->buf + tail + spsc_rbuf_header_size;
    chunk->size = header->size;
    chunk->_next = tail + spsc_rbuf_header_size + header->size;

    return true;
}

bool spsc_rbuf_consume(spsc_rbuf_t *rb, spsc_rbuf_chunk_t *chunk)
{
    InterlockedExchange64(&rb->tail, chunk->_next);
    return true;
}

void spsc_rbuf_status(spsc_rbuf_t *rb, int64_t *head, int64_t *tail)
{
    *head = InterlockedCompareExchange64(&rb->head, 0, 0);
    *tail = InterlockedCompareExchange64(&rb->tail, 0, 0);
}