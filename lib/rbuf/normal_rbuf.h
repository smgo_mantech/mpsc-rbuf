#pragma once

#include <inttypes.h>
#include <stdbool.h>

typedef struct normal_rbuf normal_rbuf_t;
typedef struct normal_rbuf_chunk {
    char   *data;
    int64_t size;
} normal_rbuf_chunk_t;
typedef struct normal_rbuf_acquired_chunk {
    char   *data;
    int64_t size;
    int64_t _prev;
    int64_t _next;
} normal_rbuf_acquired_chunk_t;

normal_rbuf_t *normal_rbuf_initialize(int64_t capacity);
void         normal_rbuf_destroy(normal_rbuf_t *rb);
int64_t      normal_rbuf_capacity(normal_rbuf_t *rb);
int64_t      normal_rbuf_size(normal_rbuf_t *rb);

void normal_rbuf_reset(normal_rbuf_t *rb);

bool normal_rbuf_acquire(normal_rbuf_t *rb, int64_t size, normal_rbuf_acquired_chunk_t *chunk);
bool normal_rbuf_push(normal_rbuf_t *rb, normal_rbuf_acquired_chunk_t *chunk);

bool normal_rbuf_acquire2(normal_rbuf_t *rb, int64_t size, normal_rbuf_acquired_chunk_t *chunk);
bool normal_rbuf_push2(normal_rbuf_t *rb, normal_rbuf_acquired_chunk_t *chunk);

bool normal_rbuf_read(normal_rbuf_t *rb, normal_rbuf_chunk_t *chunk);
bool normal_rbuf_pop(normal_rbuf_t *rb);
