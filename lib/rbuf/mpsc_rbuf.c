#include "mpsc_rbuf.h"

#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>

/**
 * @brief mpsc_rbuf Lock-free MPSC Ring buffer
 *
 * - Enqueue
 *  - acquire: 데이터를 쓰기 위해 버퍼의 포인터를 직접 할당 받는다.
 *  - Push: 데이터 쓰기를 완료한다.
 * - Dequeue
 *  - Read: 다음 데이터를 읽는다.
 *  - Pop: 데이터를 삭제한다.
 */
struct mpsc_rbuf {
    int64_t capacity;
    char   *buf;

    volatile int64_t acquired;
    volatile int64_t pushed;
    volatile int64_t read;
    volatile int64_t poped;

    // Blocking
    CRITICAL_SECTION   cs;
    CONDITION_VARIABLE cv;
};

// 데이터 상태
enum mpsc_rbuf_state {
    ACQUIRED, // 할당 됨
    OMMITED   // 사용하지 않는 영역 (버퍼의 뒷 부분)
};

#pragma pack(push, 1)
// 데이터 헤더
typedef struct mpsc_rbuf_header {
    int64_t              size;  // 데이터 길이
    enum mpsc_rbuf_state state; // 상태
} mpsc_rbuf_header_t;
#pragma pack(pop)

static int64_t mpsc_rbuf_header_size = sizeof(mpsc_rbuf_header_t);

mpsc_rbuf_t *mpsc_rbuf_initialize(int64_t capacity)
{
    mpsc_rbuf_t *rb = (mpsc_rbuf_t*)malloc(sizeof(mpsc_rbuf_t));
    rb->buf = (char*)malloc(sizeof(char) * capacity);
    rb->capacity = capacity;

    mpsc_rbuf_reset(rb);

    InitializeCriticalSection(&rb->cs);
    InitializeConditionVariable(&rb->cv);

    return rb;
}

void mpsc_rbuf_destroy(mpsc_rbuf_t *rb)
{
    if (rb != NULL) {
        if (rb->buf != NULL) {
            free(rb->buf);
        }
        free(rb);

        DeleteCriticalSection(&rb->cs);
    }
}

int64_t mpsc_rbuf_capacity(mpsc_rbuf_t *rb)
{
    return rb->capacity;
}

int64_t mpsc_rbuf_size(mpsc_rbuf_t *rb)
{
    // 아직 push 되지 않은 영역도 사용 중으로 간주한다.
    int64_t acquired = InterlockedCompareExchange64(&rb->acquired, 0, 0);
    int64_t poped = InterlockedCompareExchange64(&rb->poped, 0, 0);

    if (acquired > poped) { // [--p==a--]
        return acquired - poped;
    } else if (acquired < poped) { // [==a--p==]
        return rb->capacity - (poped - acquired);
    }
    return 0;
}

void mpsc_rbuf_reset(mpsc_rbuf_t *rb)
{
    InterlockedExchange64(&rb->acquired, 0);
    InterlockedExchange64(&rb->pushed, 0);
    InterlockedExchange64(&rb->read, 0);
    InterlockedExchange64(&rb->poped, 0);
}

bool mpsc_rbuf_acquire(mpsc_rbuf_t *rb, int64_t size, mpsc_rbuf_acquired_chunk_t *chunk)
{
    int64_t raw_size = size + mpsc_rbuf_header_size;
    int64_t acq, poped, next;
    mpsc_rbuf_header_t *header;

    for (;;) {
        acq = InterlockedCompareExchange64(&rb->acquired, 0, 0);
        poped = InterlockedCompareExchange64(&rb->poped, 0, 0);
        next = acq + raw_size;

        if (acq < poped) { // [==a--p==]
            if (next < poped) { // 기록 가능
                if (InterlockedCompareExchange64(&rb->acquired, next, acq) != acq) {
                    // acquired 값이 이미 변경되었으면 다음 영역으로 재시도
                    // printf("acq exchange1 fail %d to %d:%d\n", acq, next, size);
                    continue;
                }

                header = (mpsc_rbuf_header_t*)(rb->buf + acq);
                header->size = size;
                header->state = ACQUIRED;

                chunk->data = rb->buf + acq + mpsc_rbuf_header_size;
                chunk->size = size;
                chunk->_prev = acq;
                chunk->_next = next;
                // printf("acq exchanged1 %d to %d:%d\n", acq, next, size);
                return true;
            } else { // full
               // printf("full1\n");
               return false;
            }
        } else { // [--p==a--] or empty
            if (next + mpsc_rbuf_header_size >= rb->capacity) { // 포인터 오버플로우
                if (raw_size < poped) {
                    next = raw_size;
                    if (InterlockedCompareExchange64(&rb->acquired, next, acq) != acq) {
                        // acquired 값이 이미 변경되었으면 다음 영역으로 재시도
                        // printf("acq exchange2 fail %d to %d:%d\n", acq, next, size);
                        continue;
                    }

                    // 미사용 영역 표기
                    header = (mpsc_rbuf_header_t*)(rb->buf + acq);
                    header->size = rb->capacity - acq;
                    header->state = OMMITED;

                    // printf("acq omitted %d\n", acq);

                    header = (mpsc_rbuf_header_t*)(rb->buf);
                    header->size = size;
                    header->state = ACQUIRED;

                    chunk->data = rb->buf + mpsc_rbuf_header_size;
                    chunk->size = size;
                    chunk->_prev = acq;
                    chunk->_next = next;

                    // printf("acq exchanged2 %d to %d:%d\n", acq, next, size);
                    return true;
                } else { // full
                    // printf("full2\n");
                    return false;
                }
            } else {
                if (InterlockedCompareExchange64(&rb->acquired, next, acq) != acq) {
                    // acquired 값이 이미 변경되었으면 다음 영역으로 재시도
                    // printf("acq exchange3 fail %d to %d:%d\n", acq, next, size);
                    continue;
                }
                header = (mpsc_rbuf_header_t*)(rb->buf + acq);
                header->size = size;
                header->state = ACQUIRED;

                chunk->data = rb->buf + acq + mpsc_rbuf_header_size;
                chunk->size = size;
                chunk->_prev = acq;
                chunk->_next = next;
                // printf("acq exchanged3 %d to %d:%d\n", acq, next, size);
                return true;
            }
        }
    }
}

bool mpsc_rbuf_push(mpsc_rbuf_t *rb, mpsc_rbuf_acquired_chunk_t *chunk)
{
    // pushed를 데이터 순서대로 진행 시키기 위해 이전 값과 같을때만 변경한다.
    while (InterlockedCompareExchange64(&rb->pushed, chunk->_next, chunk->_prev) != chunk->_prev) {
        // printf("can't push %d to %d cur %d\n", chunk->_prev, chunk->_next,
        //     InterlockedCompareExchange64(&rb->pushed, 0, 0));
        SwitchToThread();
    }
    // printf("pushed %d to %d\n", chunk->_prev, chunk->_next);
    return true;
}

bool mpsc_rbuf_read(mpsc_rbuf_t *rb, mpsc_rbuf_chunk_t *chunk)
{
    int64_t head = InterlockedCompareExchange64(&rb->pushed, 0, 0);
    int64_t tail = InterlockedCompareExchange64(&rb->read, 0, 0);
    mpsc_rbuf_header_t *header;

    if (head == tail) { // Empty
        return false;
    }

    if (head > tail && head - tail < mpsc_rbuf_header_size) { // [---th---] => empty
        return false;
    }

    header = (mpsc_rbuf_header_t*)(rb->buf + tail);
    if (header->state == OMMITED) {
        // 이 상태는 포인터의 맨 끝에만 지정되므로 즉시 처음으로 이동
        if (head == 0) { // empty
            return false;
        }
        tail = 0;
        header = (mpsc_rbuf_header_t*)(rb->buf);
        // printf("read: skip ommited\n");
    }

    int64_t next = tail + mpsc_rbuf_header_size + header->size;
    chunk->data = rb->buf + tail + mpsc_rbuf_header_size;
    chunk->size = header->size;

    // printf("read at %d:%d next %d\n", tail, header->size, next);
    InterlockedExchange64(&rb->read, next);
    return true;
}

bool mpsc_rbuf_pop(mpsc_rbuf_t *rb)
{
    int64_t head = InterlockedCompareExchange64(&rb->pushed, 0, 0);
    int64_t tail = InterlockedCompareExchange64(&rb->poped, 0, 0);
    mpsc_rbuf_header_t *header;

    if (head == tail) {
        // printf("empty1\n");
        return false;
    }

    if (head > tail && head - tail < mpsc_rbuf_header_size) { // [---th---] => empty
        // printf("empty2\n");
        return false;
    }

    header = (mpsc_rbuf_header_t*)(rb->buf + tail);
    if (header->state == OMMITED) {
        // 이 상태는 포인터의 맨 끝에만 지정되므로 즉시 처음으로 이동
        if (head == 0) { // empty
            return false;
        }
        tail = 0;
        header = (mpsc_rbuf_header_t*)(rb->buf);
        // printf("skip ommited\n");
    }

    int64_t next = tail + mpsc_rbuf_header_size + header->size;
    InterlockedExchange64(&rb->poped, next);
    // printf("poped at %d:%d next %d\n", tail, header->size, next);

    return true;
}

bool mpsc_rbuf_wait(mpsc_rbuf_t *rb)
{
    EnterCriticalSection(&rb->cs);
    SleepConditionVariableCS(&rb->cv, &rb->cs, INFINITE);
    LeaveCriticalSection(&rb->cs);
}

bool mpsc_rbuf_wakeup(mpsc_rbuf_t *rb)
{
    EnterCriticalSection(&rb->cs);
    WakeConditionVariable(&rb->cv);
    LeaveCriticalSection(&rb->cs);
}