#pragma once

#include <inttypes.h>
#include <stdbool.h>

typedef struct mpsc_rbuf mpsc_rbuf_t;
typedef struct mpsc_rbuf_chunk {
    char   *data;
    int64_t size;
} mpsc_rbuf_chunk_t;
typedef struct mpsc_rbuf_acquired_chunk {
    char   *data;
    int64_t size;
    int64_t _prev;
    int64_t _next;
} mpsc_rbuf_acquired_chunk_t;

mpsc_rbuf_t *mpsc_rbuf_initialize(int64_t capacity);
void         mpsc_rbuf_destroy(mpsc_rbuf_t *rb);
int64_t      mpsc_rbuf_capacity(mpsc_rbuf_t *rb);
int64_t      mpsc_rbuf_size(mpsc_rbuf_t *rb);

void mpsc_rbuf_reset(mpsc_rbuf_t *rb);

bool mpsc_rbuf_acquire(mpsc_rbuf_t *rb, int64_t size, mpsc_rbuf_acquired_chunk_t *chunk);
bool mpsc_rbuf_push(mpsc_rbuf_t *rb, mpsc_rbuf_acquired_chunk_t *chunk);

bool mpsc_rbuf_read(mpsc_rbuf_t *rb, mpsc_rbuf_chunk_t *chunk);
bool mpsc_rbuf_pop(mpsc_rbuf_t *rb);

bool mpsc_rbuf_wait(mpsc_rbuf_t *rb);
bool mpsc_rbuf_wakeup(mpsc_rbuf_t *rb);